<?php

namespace App\Http\Controllers;

use App\Calis;
use Illuminate\Http\Request;

class CalisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Calis  $calis
     * @return \Illuminate\Http\Response
     */
    public function show(Calis $calis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Calis  $calis
     * @return \Illuminate\Http\Response
     */
    public function edit(Calis $calis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Calis  $calis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Calis $calis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Calis  $calis
     * @return \Illuminate\Http\Response
     */
    public function destroy(Calis $calis)
    {
        //
    }
}
